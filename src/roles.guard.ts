import { Injectable, CanActivate, ExecutionContext } from '@nestjs/common';
import { Reflector } from '@nestjs/core';

@Injectable()
export class RolesGuard implements CanActivate {
  constructor(private readonly reflector: Reflector) {}

  canActivate(context: ExecutionContext): boolean {
    const requiredRoles = this.reflector.getAllAndOverride<string[]>('roles', [
      context.getHandler(),
      context.getClass(),
    ]);

    if (!requiredRoles) {
      // No roles specified, allow access
      return true;
    }

    const { user } = context.switchToHttp().getRequest();
    if (!user) {
      // User not authenticated, deny access
      return false;
    }

    // Check if user has any of the required roles
    const hasRequiredRole = user.roles.some(role => requiredRoles.includes(role));
    return hasRequiredRole;
  }
}
