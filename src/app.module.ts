import { Module } from '@nestjs/common';
import { ResourceController } from './app.controller';
import { ResourceService } from './app.service';

@Module({
  controllers: [ResourceController],
  providers: [ResourceService],
})
export class AppModule {}
