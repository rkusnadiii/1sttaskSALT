import { Controller, Get, Post, Put, Delete, Param, Body, UseGuards } from '@nestjs/common';
import { JwtAuthGuard } from './jwt-auth.guard';
import { RolesGuard } from './roles.guard';
import { Roles } from './roles.decorator';

// Dummy data for demonstration purposes
const data = [
  { id: 1, name: 'Item 1' },
  { id: 2, name: 'Item 2' },
  { id: 3, name: 'Item 3' },
];

@Controller('items')
@UseGuards(JwtAuthGuard, RolesGuard)
export class ItemsController {
  @Get()
  getAllItems() {
    return data;
  }

  @Get(':id')
  getItemById(@Param('id') id: number) {
    const item = data.find(item => item.id === id);
    if (item) {
      return item;
    } else {
      throw new NotFoundException('Item not found');
    }
  }

  @Post()
  @Roles('admin')
  createItem(@Body() item: any) {
    const newItem = { id: data.length + 1, name: item.name };
    data.push(newItem);
    return { message: 'Item created' };
  }

  @Put(':id')
  @Roles('admin')
  updateItem(@Param('id') id: number, @Body() item: any) {
    const index = data.findIndex(item => item.id === id);
    if (index >= 0) {
      data[index].name = item.name;
      return { message: 'Item updated' };
    } else {
      throw new NotFoundException('Item not found');
    }
  }

  @Delete(':id')
  @Roles('admin')
  deleteItem(@Param('id') id: number) {
    const index = data.findIndex(item => item.id === id);
    if (index >= 0) {
      data.splice(index, 1);
      return { message: 'Item deleted' };
    } else {
      throw new NotFoundException('Item not found');
    }
  }
}