# BSI - SYSTEM ENVIRONMENT
BSI | KEYCLOAK , SQL SERVER , ZIPKIN , RABBIT MQ , KRAKEND

## REQUIREMENT

This requirement is only for development

| Name           | Version  | URL                                      |
|----------------|----------|------------------------------------------|
| Docker         | 20.10.17 | https://www.docker.com/                  |
| docker-compose | 2.17      | https://docs.docker.com/compose/install/ |

## SETUP
Read the [setup documentation](SETUP.md)
