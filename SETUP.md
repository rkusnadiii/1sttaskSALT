# Setup

At root project run this command to copy ```.env.example``` file into ```.env```, this environment file is use for docker configuration.
You can set the port of the container that fit with your machine and make sure the port you wanted to bind is available.

```bash
$ cp -rv .env.example .env
```

Continue to copy this file, of course you can edit fit with your needs, but I suggest you to use the default config from the example file.
```bash
$ cp -rv .docker/compose/local/keycloak.env.example .docker/compose/local/keycloak.env
$ cp -rv .docker/compose/local/mssql.env.example .docker/compose/local/mssql.env
$ cp -rv .docker/compose/local/rabbitmq.env.example .docker/compose/local/rabbitmq.env
```

Finally, run this command, and you are ready to go ! Happy coding :)
```bash
$ chmod +x runner.sh
$ ./runner.sh -e=local -a=start
```
